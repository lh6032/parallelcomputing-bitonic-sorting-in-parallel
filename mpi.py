#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 21:12:32 2020
This program implements bitonic sorting in parallel by calling MPI. The number 
of processors are defined in the command, all processors have shared memory 
which is created in rank 0. The original dataset is given randomly, and then 
applying bitonic sort based on bitonic sequence.

@author: SophieHou (lh6032)
"""
# pip install mpi mpi4py
# command: mpirun -n 4 python mpi.py # on a laptop
# or mpiexec


from mpi4py import MPI
import numpy as np
import sys
import time

comm = MPI.COMM_WORLD
numproces = comm.Get_size() # num of processors
rank = comm.Get_rank() # processor name
 

def generateData(size, data):
    """
    Generate raw datasets randomly.

    Parameters
    ----------
    size : int, the length of list
    data : list, original dataset

    Returns
    -------
    data : list, original dataset
    """
    data=[]
    np.random.seed(20) 
    for i in range(size):
        item = np.random.randint(0, size)
        data.append(item)
    print("Original Dataset:", data)
    return data


def bitonicSeq(data):
    """
    Convert original dataset to bitonic Sequence.
    
    Parameters
    ----------
    data : list, original dataset

    Returns
    -------
    modified : list, bitonic sequence
    """
    modified=[]
    mid=len(data)//2
    modified.extend(sorted(data[:mid],key=int))
    modified.extend(sorted(data[mid:],key=int,reverse=True))
    print("Bitonic Seq:", modified)
    return modified            
        
    
def swap(data,l,r):
    """
    If the previous one is bigger than the latter, then swap
    them to make a set in ascending order.

    Parameters
    ----------
    data : list, dataset
    l : int, previous index of list
    r : int, next index of list

    Returns
    -------
    None.

    """
    if data[l] > data[r]:
        #print("swap--", l,r,data, " rank=",rank) # Check
        data[l],data[r] = data[r],data[l]
        

def sort(data, low, size, split):
    """
    Each processor handles 1 to more pairs of elements, check each pair
    and compare them. If in one pair, the previous element is bigger than
    the latter one, then swap.

    Parameters
    ----------
    data : list
    low : the first index of the range of elements that one processor handled. 
    size : the total elements that one processor will handle
    split : the interval between a pair of elements

    Returns
    -------
    None.

    """
    if split>=1:
        for i in range(low, low+size):
            if i+split < len(data):
                swap(data,i, i+split)
    

def recurSort(data, lo, size, length, rrank):
    """
    Performs bitonic sorting algorithm. 

    Parameters
    ----------
    data : list, target dataset
    lo : int, the first index of the range of elements that one processor holds.
    size : int, the range of elements that one processor holds. 
    length : int, in each recursive, the interval between one pair of elements.
    rrank : int, rank name in bin
    
    Returns
    -------
    None.

    """
    if length>=1:
        split = length//2
        sort(data, lo, size,split)
        #print("data=",data, "lo=",lo, 'size=',size, 'split=', split, ", my rank=", rank) #check
        if rrank and rrank[-1]=='1':
            lo+=split//2
        rrank=rrank[:-1]
        comm.Barrier()
        recurSort(data, lo, size, split,rrank)
        recurSort(data, lo+split, size, split, rrank)


def main(argv,runtime):
    start = time.time() # start time
    
    if len(argv) < 2:
        print("Command Error! Please Input Size of Array!")
    SIZE = int(argv[1]) # size of data
    resp = SIZE//numproces # the number of elements that each processor holds 
    if rank==0:
        shared = numproces * SIZE**2  # set shared size
    else:
        shared = 0
    # create a share memory in rank 0
    window = MPI.Win.Allocate_shared(shared, numproces,comm=comm)
    # target array created in shared memory
    buff, numprocess = window.Shared_query(0)
    assert numprocess == comm.Get_size()
    data = np.ndarray(buffer=buff, dtype='i', shape=(SIZE,))

    if rank==0:    
        data[:SIZE] = bitonicSeq(generateData(SIZE, data)) # create bitonic sequence

    l = rank * (resp//2) # the first index of arange of elements handled by each processor
    bsize = len(bin(numprocess))-3
    rrank = bin(rank)[2:].rjust(bsize,'0')[::-1]
    comm.Barrier()
    recurSort(data, l, resp//2, SIZE,rrank)  # rum bitonic sorting algorithm
    comm.Barrier()
    
    if rank==0:
        print("Sorted Dataset: ", data[:SIZE])
        print("Runtime: ",  np.round(time.time()-start,3))
        runtime.append(np.round(time.time()-start,3))
        


if __name__ == '__main__':
    runtime=[]
    for i in range(19): # check 19 runs
        main(sys.argv, runtime)
    if rank==0:
        print("\n\nMean of 19 runs:", np.mean(runtime))










