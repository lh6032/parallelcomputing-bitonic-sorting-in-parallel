When compiling and executing this python program, few things need to be noted: 
1). Install mpi,mpi4py. Users can install in Anaconda with below sentence:
    pip install mpi mpi4py dumpy time
2). Run the program in terminal with command line: 
    mpirun -n 4 python mpi.py 1024
Note: 
--mpirun: or mpiexec 
--4: the number of processors, users can change to 8, or 2
--1024: the size of dataset, it can only be the number that is the power of 2

3). The output displays 3 kinds of datasets on the terminal:
--original dataset
--bitonic sequence
--sorted dataset

